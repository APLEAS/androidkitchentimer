package com.example.apleas.apleastimesaticking;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class MainActivity extends Activity {
    MainView animView;
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button newTimer = (Button) findViewById(R.id.newTime);
        final Button oldTimer = (Button) findViewById(R.id.oldTime);

//handles the DVR key starts appropriate activity
        newTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =
                        new Intent(MainActivity.this, NewTimer.class);
                startActivity(intent);
            }
        });
//handles the configure key starts appropriate activity
        oldTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =
                        new Intent(MainActivity.this, OldTimer.class);
                startActivity(intent);
            }
        });

        animView = (MainView) findViewById(R.id.v1);
    }

    public void restart() {
        animView.restart((Integer) spinner.getSelectedItem());
    }

    @Override
    protected void onResume() {
        super.onResume();
        animView.startAnimation();
    }

    @Override
    protected void onPause() {
        super.onPause();
        animView.stopAnimation();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
