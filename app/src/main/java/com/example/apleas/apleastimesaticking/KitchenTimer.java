package com.example.apleas.apleastimesaticking;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import android.os.PowerManager;

public class KitchenTimer extends Activity {
    protected PowerManager.WakeLock mWakeLock;
    private int indexer = 1000;
    private SparseBooleanArray test = new SparseBooleanArray();
    private SparseBooleanArray alarmChecker = new SparseBooleanArray();
    private int id;
   private ArrayList<Time> timeList = new ArrayList();
    private MyDBHandler db = new MyDBHandler(this);
    private String recipe;
    private final HashMap <Integer, TextView> timeMap = new HashMap<Integer, TextView>();
    final ArrayList<Time> t = new ArrayList();
    TextView name, time;
    Button btn, done;
    private int count = 0;
    TextView description;
    ArrayList<Long> display = new ArrayList();
   private Handler handler = new Handler();
    public Uri notification;
    public Ringtone r;
ArrayList<Ringtone> ring = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayout masterLayout = new TableLayout(this);
        TableLayout tl = new TableLayout(this);
        tl.setStretchAllColumns(true);

        tl.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT));
        tl.setWeightSum(3);
        tl.setPadding(30,30,30,30);
        final Button back = new Button(this);
        name = (TextView) findViewById(R.id.name);
        onStart();
         notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);

            r = RingtoneManager.getRingtone(getApplicationContext(), notification);
        for (int q = 0; q < timeList.size(); q++)
            ring.add(r);
        TableRow.LayoutParams descripParams = new TableRow.LayoutParams(
                TableRow.LayoutParams.WRAP_CONTENT   , TableRow.LayoutParams.WRAP_CONTENT);
      //  descripParams.gravity = Gravity.LEFT;

        LinearLayout.LayoutParams masterParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        TableRow.LayoutParams timeParams = new TableRow.LayoutParams(
                TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        timeParams.gravity = Gravity.RIGHT;
        timeParams.setMargins(0,0,20,0);

        TableRow.LayoutParams buttonParam = new TableRow.LayoutParams(
                TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        buttonParam.gravity = Gravity.LEFT;

        TableRow.LayoutParams rowParam = new TableRow.LayoutParams(
                TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1.0f);
        buttonParam.gravity = Gravity.LEFT;

       rowParam.setMargins(0, 10, 0, 10);
        rowParam.gravity = Gravity.FILL;
        final TextView recipeName = new TextView(this);
        recipeName.setText(recipe);
        recipeName.setTextSize(50);

        done = new Button(this);



        for (int i = 0; i < t.size(); i++) {

            TextView time = new TextView(this);
            Time r = t.get(i);
            List<String> listOfTimes = Arrays.asList(r.getNum().split(":"));
            int hr = Integer.parseInt(listOfTimes.get(0));
            int m = Integer.parseInt(listOfTimes.get(1));
            int s = Integer.parseInt(listOfTimes.get(2));
            long hour = TimeUnit.HOURS.toMillis(hr);
            long min = TimeUnit.MINUTES.toMillis(m);
            long sec = TimeUnit.SECONDS.toMillis(s);

            long total = hour + min + sec;
            display.add(total);
            time.setText(r.getNum());
            time.setTextSize(25);
            time.setGravity(Gravity.LEFT);

            time.setTypeface(Typeface.SERIF, Typeface.BOLD);
            time.setLayoutParams(timeParams);
            timeMap.put(i, time);

            TableRow rows = new TableRow(this);
            rows.setLayoutParams(rowParam);
            btn = new Button(this);
            btn.setId(i);
            btn.setText("START");
            rows.addView(btn);
            Time row = t.get(i);
            description = new TextView(this);
            description.setLayoutParams(rowParam);

            description.setSingleLine(false);

            description.setText(row.getDescription());
            description.setMaxEms(5);
            description.setTextSize(13);
            description.setGravity(Gravity.FILL);
            description.setId(indexer);
            rows.addView(description);
            indexer += 1;
            rows.addView(timeMap.get(i));
            tl.addView(rows);

            View.OnClickListener Listener = new View.OnClickListener() {
                public void onClick(View v) {
                    Button b = (Button) v;
                    id = b.getId();

                    String state = b.getText().toString();
                    btn = (Button) findViewById(id);
                    Log.d("INDEX", "index ==   " + id);
                    if (state.contains("STOP")) {
                        test.put(id, false);
                        btn.setText("START");

                        boolean val = alarmChecker.valueAt(id);
                        if (!val) {
                            ring.get(id).stop();
                        }

                    }

                    if (state.contains("START")) {
                        alarmChecker.put(id, true);
                        test.put(id, true);
                        btn.setText("STOP");
                        count += 1;
                        if (count <= 1) {
                            handler.postDelayed(runner, 500);
                        }
                    }
                    if (state.contains("DONE")) {
                        handler.removeCallbacks(runner);

                        finish();
                        System.exit(0);
                        }

                }

            };
            btn.setOnClickListener(Listener);
            done.setOnClickListener(Listener);
        }

        TableRow rows = new TableRow(this);
        rows.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1.0f));

        done.setText("DONE");

        rows.addView(done);
        tl.addView(rows);
        masterLayout.addView(tl);
       // masterLayout.setBackgroundColor(Color.BLUE);
        setContentView(masterLayout);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    Runnable runner = new Runnable() {
        @Override
        public void run() {
            updateTime();

        }
    };

 public synchronized void updateTime()   {
     for (int k = 0; k <= t.size(); k++) {
        if (test.get(k) && display.get(k) > 0) {
            Long num = display.get(k) - 1000;
            display.set(k, num);

                long second = (num / 1000) % 60;
                long minute = (num / (1000 * 60)) % 60;
                long hour = (num / (1000 * 60 * 60)) % 24;

                String time = String.format("%02d:%02d:%02d", hour, minute, second);
                timeMap.get(k).setText(time);
if(display.get(k) == 0) {

        timeMap.get(k).setTextColor(Color.RED);
description = (TextView) findViewById(k + 1000);
description.setTextColor(Color.RED);
    try {
        alarmChecker.put(k, false);
        ring.get(k).play();
    } catch (Exception e) {
        e.printStackTrace();
    }
}
        }
    }
    handler.postDelayed(runner, 1000);
 }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_timer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onStart() {
        super.onStart();
        // The activity is about to become visible.
        Intent intent = getIntent();
        if (intent != null) {
            recipe = (String) intent.getCharSequenceExtra("recipe");
            getTimes();


            //  bio.setText(intent.getFloatExtra("Bio ", ));
        }
    }

    private void getTimes() {
        timeList = db.findRecipe(recipe);
        t.addAll(timeList);


    }
    private void setName(){
        name.setText(Integer.toString(timeList.size()));
    }



public class kitchenCountdownTimer extends CountDownTimer {

    public kitchenCountdownTimer(long startTime, long interval) {
        super(startTime, interval);
    }

    @Override
    public void onFinish() {
        //  text.setText("Time's up!");
        //   timeElapsedView.setText("Time Elapsed: " + String.valueOf(startTime));
    }



    @Override
    public void onTick(long millisUntilFinished)
    {
        time.setText("Time remain:" + millisUntilFinished);

    }
}



}