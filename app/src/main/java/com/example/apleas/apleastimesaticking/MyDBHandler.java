package com.example.apleas.apleastimesaticking;

/**
 * Created by APLEAS on 11/6/2015.
 */
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;
import android.content.ContentValues;
import android.database.Cursor;
import android.widget.Toast;

import java.util.ArrayList;

public class MyDBHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "recipeBook.db";
    private static String TABLE_RATING = "RATINGS";
    private static String TABLE_RECIPE = "recipe";

   public static final String COLUMN_NAME = "recipeName";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_TIME = "time";
    public static final String COLUMN_RATING = "rating";

    private Cursor find;
    private Cursor list;

    public MyDBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_RECIPE_TABLE = "CREATE TABLE " +
                TABLE_RECIPE + "(" + COLUMN_DESCRIPTION
                + " TEXT," + COLUMN_TIME + " TEXT" + ")";
        db.execSQL(CREATE_RECIPE_TABLE);

        String CREATE_RECIPE_RATING = "CREATE TABLE " +
                TABLE_RATING + "(" + COLUMN_NAME
                + " TEXT UNIQUE," + COLUMN_RATING + " TEXT" + ")";
        db.execSQL(CREATE_RECIPE_RATING);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion,
                          int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECIPE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RATING);
        onCreate(db);
    }

    public String addTime(Time t) {
        String recipe = t.getRecipe().replaceAll(" ", "_");

        ContentValues values = new ContentValues();
        TABLE_RECIPE = recipe;
        values.put(COLUMN_DESCRIPTION, t.getDescription());
        values.put(COLUMN_TIME, t.getNum());


        SQLiteDatabase db = this.getWritableDatabase();
        String ADD_TABLE = "CREATE TABLE IF NOT EXISTS " +
                TABLE_RECIPE + "(" + COLUMN_DESCRIPTION
                + " TEXT," + COLUMN_TIME + " TEXT" + ")";
        db.execSQL(ADD_TABLE);

String s = t.getDescription();
        db.insert(TABLE_RECIPE, null, values);
        db.close();
        return s;
    }
    public void deleteRecipe(String recipeName) {
        SQLiteDatabase db = this.getWritableDatabase();
       db.execSQL("DROP TABLE IF EXISTS " + recipeName);

    }

    public ArrayList<Time> findRecipe(String recipeName) {
        String query = "Select * FROM " + recipeName;
        SQLiteDatabase db = this.getWritableDatabase();

        list = db.rawQuery(query, null);
        ArrayList<Time> recipe = new ArrayList();
        if (list.moveToFirst()) {
            Time t = new Time(recipeName, list.getString(list.getColumnIndex("description")), list.getString(list.getColumnIndex("time")));
            recipe.add(t);
            while (list.moveToNext()) {
                Time time = new Time(recipeName, list.getString(list.getColumnIndex("description")), list.getString(list.getColumnIndex("time")));
                recipe.add(time);

            }
            list.close();
        } else {

        }
        db.close();
        return recipe;
    }

    public ArrayList<String> getRecipeTitles() {
        String query = "SELECT name FROM sqlite_master WHERE type='table' ";
        SQLiteDatabase db = this.getWritableDatabase();
        find = db.rawQuery(query, null);
        ArrayList<String> title = new ArrayList();
        if (find.moveToFirst()) {
            while (find.moveToNext()) {
                if(!find.getString(find.getColumnIndex("name")).contains("RATINGS"))
                title.add(find.getString(find.getColumnIndex("name")));
            }
            find.close();
        }

        return title;

    }

    public void addRating(String recipe, String rating) {
        if(recipe.contains(" ")) {
            recipe = recipe.replaceAll(" ", "_");
        }
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, recipe);
        values.put(COLUMN_RATING, rating);
        SQLiteDatabase db = this.getWritableDatabase();


        String ADD_TABLE = "CREATE TABLE IF NOT EXISTS " +
                TABLE_RATING + "(" + COLUMN_NAME
                + " TEXT," + COLUMN_RATING + " TEXT" + ")";

        db.execSQL(ADD_TABLE);

       db.insert(TABLE_RATING, null, values);
        db.close();
    }

    public void updateRating(String recipe, String rating){
        if(recipe.contains(" ")) {
            recipe = recipe.replaceAll(" ", "_");
        }
        ContentValues views = new ContentValues();
        views.put(COLUMN_NAME, recipe);
        views.put(COLUMN_RATING, rating);
        SQLiteDatabase db = this.getWritableDatabase();
        String where = "recipeName = ?";
        String[] wherearg = new String[1];
        wherearg[0] = recipe;
        db.update(TABLE_RATING, views, where , wherearg);
        db.close();

    }

    public String getRating(String recipe){
        String query = "SELECT rating FROM RATINGS WHERE recipeName == '" +recipe +"'";
        SQLiteDatabase db = this.getWritableDatabase();
        find = db.rawQuery(query, null);
        find.moveToFirst();
        String rating = find.getString(find.getColumnIndex("rating"));


        return rating;
    }
}