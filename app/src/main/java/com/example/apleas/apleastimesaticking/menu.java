package com.example.apleas.apleastimesaticking;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

public class menu extends Activity {
Button multi, single, delete, main;
TextView title;
    public String recipe;
    private MyDBHandler db = new MyDBHandler(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        onStart();
        main = (Button) findViewById(R.id.main);
        title = (TextView) findViewById(R.id.title);
        multi = (Button) findViewById(R.id.multi);
        single = (Button) findViewById(R.id.single);
        delete = (Button) findViewById(R.id.delete);
        recipe = recipe.replaceAll("_", " ");
        title.setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
title.setText(recipe);


        View.OnClickListener Listener = new View.OnClickListener() {
            public void onClick(View v) {
                RatingBar ratingBar = (RatingBar) findViewById(R.id.rating);
                float f = ratingBar.getRating();
                if(f > 0){
                    String s = Float.toString(f);
                    db.updateRating(recipe, s);

                }

                Button b = (Button) v;

                String state = b.getText().toString();

                if(state.contains("MULTIPLE")) {
                    Intent intent = new Intent(menu.this, KitchenTimer.class);
                    intent.putExtra("recipe", recipe);
                    startActivity(intent);
                }

                if (state.contains("SINGLE")) {
                    Intent intent = new Intent(menu.this, OneTimer.class);
                    intent.putExtra("recipe", recipe);
                    startActivity(intent);

                }


                if (state.contains("DELETE")) {
                    db.deleteRecipe(recipe);
                    Intent intent = new Intent(menu.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                }
                if (state.contains("MAIN")) {
                    Intent intent = new Intent(menu.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);


                }
            }

        };
        main.setOnClickListener(Listener);
        single.setOnClickListener(Listener);
        multi.setOnClickListener(Listener);
        delete.setOnClickListener(Listener);
    }



    @Override
    public void finish() {
        RatingBar ratingBar = (RatingBar) findViewById(R.id.rating);
        float f = ratingBar.getRating();
        String s = Float.toString(f);
        db.updateRating(recipe, s);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_menue, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        if (intent != null) {
            recipe = (String) intent.getCharSequenceExtra("recipe");
                TextView name = (TextView) findViewById(R.id.text1);
                RatingBar rating = (RatingBar) findViewById(R.id.rating);

                String recipe = (String) intent.getCharSequenceExtra("recipe");
                String s = db.getRating(recipe);
                float rate = Float.parseFloat(s);
                rating.setRating(rate);
            }

        }

}
