package com.example.apleas.apleastimesaticking;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class NewTimer extends Activity implements View.OnClickListener {
    SQLiteDatabase db;

    private ArrayList <String> descrip = new ArrayList();
    private String recipe;
    private String time;
    private String describe;
    private Time t;
    HashMap<String, HashMap<String, String>> recipeBook = new HashMap();
    HashMap<String, String> steps = new HashMap();
NumberPicker hr, min, sec;
Button save, more;
    EditText title, description ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_timer);

        hr = (NumberPicker) findViewById(R.id.hr);
        min = (NumberPicker) findViewById(R.id.min);
        sec = (NumberPicker) findViewById(R.id.sec);
        title = (EditText) findViewById(R.id.t);
        description = (EditText) findViewById(R.id.d);
        save = (Button) findViewById(R.id.bsave);
        more = (Button) findViewById(R.id.bmore);
        db = openOrCreateDatabase("RECIPE_BOOK", Context.MODE_PRIVATE, null);
        hr.setMaxValue(100);
        hr.setMinValue(0);
        hr.setWrapSelectorWheel(true);
        min.setMaxValue(60);
        min.setMinValue(0);
        min.setWrapSelectorWheel(true);
        sec.setMaxValue(60);
        sec.setMinValue(0);
        sec.setWrapSelectorWheel(true);

        View.OnClickListener Listener = new View.OnClickListener() {
            public void onClick(View v) {

                int hour = 0;
                int minute = 0;
                int second = 0;
                int total = 0;
if (v instanceof EditText){
    hideSoftKeyboard(v);
}

                        Button b = (Button) v;

                        String s = b.getText().toString().trim();
                        if (s.contains("SAVE RECIPE") || s.contains("ADD")) {
                            hour = hr.getValue();
                            minute = min.getValue();
                            second = sec.getValue();
                            total = hour + minute + second;

                            if (total <= 0 ||
                                    title.getText().toString().trim().length() == 0) {
                                return;
                            }
                            String h = Integer.toString(hour);
                            String m = Integer.toString(minute);
                            String ss = Integer.toString(second);
                            time = h + ":" + m + ":" + ss;
                            recipe = title.getText().toString().trim();
                            describe = description.getText().toString().trim();
                            descrip.add(describe);

                            steps.put(describe, time);
                            t = new Time(recipe, describe, time);
                            addTime(t);

                        }
                        if (s.contains("SAVE RECIPE")) {
                            recipeBook.put(recipe, steps);
                            steps.put(describe, time);
                            System.exit(0);

                        } else {

                            description.setText("");
                            hr.setValue(0);
                            min.setValue(0);
                            sec.setValue(0);

                        }
                    }

        };


        save.setOnClickListener(Listener);
        more.setOnClickListener(Listener);

    }


    public void hideSoftKeyboard(View view){
        InputMethodManager imm =(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public void addTime(Time tt){
        MyDBHandler dbHandler = new MyDBHandler(this);
        dbHandler.addTime(tt);
        dbHandler.addRating(tt.getRecipe(), "0");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_timer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

    }
}
