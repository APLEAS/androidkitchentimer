package com.example.apleas.apleastimesaticking;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by APLEAS on 11/3/2015.
 */
public class Timers {
    HashMap<String, ArrayList<Time>> times;
    String recipe;


    public Timers(String newRecipe, String newDescription, Time newT){

        setTime(newRecipe, newDescription, newT);
    }



private void setTime(String r, String d, Time t){
    recipe = r;
   times.get(r).add(t);
    
}
    private void setRecipe(String newRecipe) {
        recipe = newRecipe;
    }


    public HashMap<String, ArrayList<Time>> getTimes(){
        return times;
    }
    public String getRecipe(){
        return recipe;
    }
    public String toString(){
        return recipe + "" ;
    }

}
