package com.example.apleas.apleastimesaticking;

import android.app.Activity;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;



public class OneTimer extends Activity {
    private int id;
    private long step = 0;
    private long total = 0;
    private long grandTotal=0;
    private ArrayList<Time> timeList = new ArrayList();
    private MyDBHandler db = new MyDBHandler(this);
    private String recipe;
    final ArrayList<Time> t = new ArrayList();
    private int sleepNum = 0;
    boolean stopCheck;
    private boolean alarm;
    int stepCount = 0;
    double d;
    public Uri notification;
    public Ringtone r;
    TextView totalTime, nextStep, name, text, nextD;
    Button button, done;
    private Handler handler = new Handler();
private int progCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_timer);
        onStart();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);

        r = RingtoneManager.getRingtone(getApplicationContext(), notification);
        grandTotal = total;
        d = Math.ceil(100.00 / (grandTotal/1000.00));
        sleepNum = (int) d;
        step=timeList.get(stepCount).getLong();
        text = (TextView) findViewById(R.id.t);
        button = (Button) findViewById(R.id.bTime);
        done = (Button) findViewById((R.id.done));
        totalTime = (TextView) findViewById(R.id.time);
        nextStep = (TextView) findViewById(R.id.nextStep);
        nextD = (TextView) findViewById(R.id.nextD);
        name = (TextView) findViewById(R.id.name);
        if (recipe.contains("_")) {
            recipe = recipe.replaceAll("_", " ");
        }
        name.setText(recipe);
        text.setText("Current Step:  " + timeList.get(0).getDescription());
        totalTime.setText( formatTime(total));
        if(timeList.size()>1) {
            nextD.setText("Your next step is " + timeList.get(1).getDescription());
            nextStep.setText(formatTime(step));
        }else{
            nextD.setText("There are no more steps " );
            nextStep.setText("");
        }



            View.OnClickListener Listener = new View.OnClickListener() {
                public void onClick(View v) {
                    Button b = (Button) v;
                    id = b.getId();

                    String state = b.getText().toString();
                    Log.d("INDEX", "index ==   " + id);
                    if (state.contains("STOP")) {
                        if (alarm) {
                            r.stop();
                        }
                        stopCheck = true;
                        //        test.put(id, false);
                        button.setText("START");
                        handler.removeCallbacks(runner);

                    }

                    if (state.contains("START")) {
                        stopCheck = false;
                        button.setText("STOP");
                        if (progCount < 1) {
                            progCount += 1;
                        }
                        handler.postDelayed(runner, 500);


                    }
                    if (state.contains("DONE")) {
                        handler.removeCallbacks(runner);

                        finish();
                        System.exit(0);

                    }
                }

            };
            button.setOnClickListener(Listener);
        done.setOnClickListener(Listener);
        }





    Runnable runner = new Runnable() {
        @Override
        public void run() {
            updateTime();

        }
    };



    public synchronized void updateTime()   {
if (step == 0) {
    if (stepCount < timeList.size() -2) {
        stepCount += 1;
        step = timeList.get(stepCount).getLong();
        text.setText("Current Step:  " + timeList.get(stepCount).getDescription() + "   " + "Total Time Remaining  :   ");
        nextD.setText("Your next step is " + timeList.get(stepCount+1).getDescription());
        nextStep.setText(formatTime(step));
    }else {

        nextD.setText("No more steps");

    }
}

    if (total > 0 && step > 0) {


total -= 1000;
        step -= 1000;



       String s = formatTime(total);
        String next = formatTime(step);

        totalTime.setText(s);
        nextStep.setText(next);


    }
        if(total >0 && step <=0){
            total -= 1000;
            String s = formatTime(total);
            totalTime.setText(s);

        }
        if(total==0){
            r.play();
            alarm = true;
            return;
        }


    handler.postDelayed(runner, 1000);
}



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_timer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

private String formatTime(long num){
    long second = (num / 1000) % 60;
    long minute = (num / (1000 * 60)) % 60;
    long hour = (num / (1000 * 60 * 60)) % 24;

    String time = String.format("%02d:%02d:%02d", hour, minute, second);
    return time;
}

    @Override
    protected void onStart() {
        super.onStart();
        // The activity is about to become visible.
        Intent intent = getIntent();
        if (intent != null) {
            recipe = (String) intent.getCharSequenceExtra("recipe");
            getTimes();

        }
    }

    private void getTimes() {
        timeList = db.findRecipe(recipe);
        t.addAll(timeList);
        long l = 0;
        for (int i=0; i < timeList.size(); i ++){

            l += timeList.get(i).getLong();
            total = l;
        }




    }
    private void setName(){
        name.setText(Integer.toString(timeList.size()));
    }






}