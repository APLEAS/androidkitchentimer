package com.example.apleas.apleastimesaticking;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class OldTimer extends ListActivity {
        private static final String TAG = "MyActivity";
    private Intent starterIntent;
        private static final int RATING = 100;
        final private MyDBHandler db = new MyDBHandler(this);
    private static ArrayList<String> RECIPES = new ArrayList();;
    private static String[] list;
    public static ArrayList<String> t = new ArrayList();
    @Override
        protected void onCreate(Bundle savedInstanceState) {
        starterIntent = getIntent();
        RECIPES = db.getRecipeTitles();
            super.onCreate(savedInstanceState);
            setListAdapter(new RecipeAdapter(this));

        }

        @Override
        protected void onListItemClick(ListView l, View v, int position, long id) {

            Log.d(TAG, "onListItemClick position=" + position + " id=" + id + " " + RECIPES.get(position));
            Intent intent = new Intent(OldTimer.this, menu.class);
            intent.putExtra("recipe", RECIPES.get(position));
            startActivityForResult(intent, RATING);
        }

        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            finish();
            startActivity(starterIntent);
            Log.d(TAG, "onActivityResult");
            if (requestCode == RATING) {
                if (resultCode == RESULT_OK && data != null) {
                    ((RecipeAdapter) getListAdapter()).notifyDataSetChanged();
                }
            }
        }


        static class RecipeAdapter extends BaseAdapter {
            private LayoutInflater inflater;

            RecipeAdapter(Context context) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }

            @Override
            public int getCount() {
                return RECIPES.size();

            }



            @Override
            public Object getItem(int i) {
                return RECIPES.get(i);
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                ViewHolder holder;
                View row = convertView;
                if (row == null) {
                    row = inflater.inflate(R.layout.recipe_list, parent, false);
                    holder = new ViewHolder();
                    holder.name = (TextView) row.findViewById(R.id.name);
                //    holder.rate = (TextView) row.findViewById(R.id.rating);
                    row.setTag(holder);
                } else {
                    holder = (ViewHolder) row.getTag();
                }
String r = RECIPES.get(position);
        if (r.contains("_")) {
            r = r.replaceAll("_", " ");
        }
                holder.name.setTextSize(30);
                holder.name.setText(r);

                return row;
            }


            static class ViewHolder {

                TextView name;
           //     Button bRate;
           //     TextView rate;
            }
        }


        @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_timer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

   // @Override
    public void onClick(View v) {

    }
    private static void getSize(){
   //     t = db.getRecipeTitles();
        list = new String[t.size()];
        list = t.toArray(list);



    }





}

