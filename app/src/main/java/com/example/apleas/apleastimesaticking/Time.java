package com.example.apleas.apleastimesaticking;

import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by APLEAS on 11/3/2015.
 */
public class Time implements Serializable {
    String recipe;
    String description;
    String num;
    float rating = 4.0f;


    public Time(String newRecipe, String newDescription, String newNum){
        setDescription(newDescription);
        setNum(newNum);
        setRecipe(newRecipe);
    }

   public void setDescription(String newDescription){
        description = newDescription;
    }

    public void setRecipe(String newRecipe){

    recipe = newRecipe;
    }

    public void setNum(String newNum){
        num = newNum;
    }

    public String getRecipe(){
        return recipe;
    }
    public String getDescription(){
        return description;
    }

    public String getNum(){
        return num;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public long getLong(){

        List<String> s = Arrays.asList(num.split(":"));
            int hr = Integer.parseInt(s.get(0));
            int m = Integer.parseInt(s.get(1));
            int se = Integer.parseInt(s.get(2));
            long hour = TimeUnit.HOURS.toMillis(hr);
            long min = TimeUnit.MINUTES.toMillis(m);
            long sec = TimeUnit.SECONDS.toMillis(se);

            long total = hour + min + sec;


        return total;
    }
}
