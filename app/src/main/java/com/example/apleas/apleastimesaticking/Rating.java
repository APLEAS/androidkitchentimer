package com.example.apleas.apleastimesaticking;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class Rating extends Activity {

    private static final String TAG = "RatingsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        if (intent != null) {
            RatingBar rating = (RatingBar) findViewById(R.id.rating);
            String s = (String)intent.getCharSequenceExtra("rating");
            float rate = Float.parseFloat(s);
            rating.setRating(rate);
        }
    }

    @Override
    public void finish() {
        Log.d(TAG, "finish()");

        Intent ratingResult = new Intent();
        RatingBar ratingBar = (RatingBar) findViewById(R.id.rating);
        ratingResult.putExtra("RecipeRating", ratingBar.getRating());
        setResult(RESULT_OK, ratingResult);
        super.finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.rating, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
