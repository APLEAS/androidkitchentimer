package com.example.apleas.apleastimesaticking;

import java.util.ArrayList;

/**
 * Created by APLEAS on 11/3/2015.
 */
public class RecipeBook {
    ArrayList<Timers> book;

    public RecipeBook(Timers t){
        setRecipe(t);
    }

    private void setRecipe(Timers t){
        book.add(t);
    }

    public ArrayList<Timers> getBook(){
        return book;
    }
}
